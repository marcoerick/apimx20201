var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson=require('request-json');
var urlClientes="https://api.mlab.com/api/1/databases/mbecerra/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlUsuarios="https://api.mlab.com/api/1/databases/mbecerra/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlRaizMLab="https://api.mlab.com/api/1/databases/mbecerra/collections";
var urlTokenBBVA="https://connect.bbva.com/token?grant_type=client_credentials";
var apiKey="apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlabRaiz;
var keyBBVA="YXBwLmJidmEuZXN0YWRpc3RpY2FzOlFLeElOTlZVMVhMVkEzMW8kUlhnZmZmWVJWJFk2KlpAcEdHc2Vqekdhd1RyZnJBUyRPJURXT2VMRUlwRlhReWY=";
var urlTiles="https://apis.bbva.com/paystats_sbx/4/tiles/";
var urlZip="https://apis.bbva.com/paystats_sbx/4/zipcodes/";

var bodyParser=require('body-parser');
app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
  next();
});
var movimientosJson=require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get ('/',function(req,res){
  res.sendFile(path.join(__dirname,'index.html'));
})

app.get ('/Clientes/:idCliente',function(req,res){
  res.send('Aqui tienes el cliente numero :'+req.params.idCliente);
})

app.get ('/Categories',function(req,res){
    res.set ("Access-Control-Allow-Headers","Content-Type");
  res.sendfile('categories.json');
})

app.get ('/v2/movimientos/:indice',function(req,res){
  console.log(req.params.indice);
  res.json(movimientosJson[req.params.indice]);
})

app.get ('/v3/movimientosquery',function(req,res){
  console.log(req.query);
  res.send('Recibido');
})

app.post ('/',function(req,res){
  res.send('Hemos recibido su peticion POST');
})

app.post ('/v3/movimientos',function(req,res){
  var nuevo=req.body;
  nuevo.id=movimientosJson.length+1;
  movimientosJson.push(nuevo);
  res.send('Movimiento dado de alta');
})

app.put ('/',function(req,res){
  res.send('Hemos recibido su peticion PUT cambiada');
})


app.delete ('/',function(req,res){
  res.send('Hemos recibido su peticion DELETE');
})
/*
app.get ('/select',function(req,res){
  var clienteMLab=requestjson.createClient(urlClientes);
  clienteMLab.get('',function(err, resM, body){
    if(err){
      console.log(body);
    }else{
      res.send(body);
    }
  })
})*/

app.get ('/select',function(req,res){
  var email=req.body.email;
  var password=req.body.password;
  var query='q={"email":"'+email+'","password":"'+password+'"}';
  var clienteMLab=requestjson.createClient(urlRaizMLab+"/Usuarios?"+apiKey+"&"+query);
  clienteMLab.get('',function(err, resM, body){
    if(!err){
      if(body.length==1){
          res.status(200).send('Usuario existente');
      }else{
        res.status(404).send('Usuario no encontrado');
      }
    }
  })
})

app.post('/login',function(req,res){
  res.set ("Access-Control-Allow-Headers","Content-Type");
  var email=req.body.email;
  var password=req.body.password;
  var query='q={"email":"'+email+'","password":"'+password+'"}';
  var clienteMLabRaiz=requestjson.createClient(urlRaizMLab+"/Usuarios?"+apiKey+"&"+query);
  console.log(urlRaizMLab+"/Usuarios?"+apiKey+"&"+query);
  clienteMLabRaiz.get('',function(err,resM,body){
    if(!err){
      if(body.length==1){
          res.status(200).send(body[0]._id);
      }else{
        res.status(404).send('Usuario no encontrado, registrese');
      }
    }
  })
})

app.post('/create',function(req,res){
  res.set ("Access-Control-Allow-Headers","Content-Type");
  var email=req.body.email;
  var password=req.body.password;
  var params={"email":email,"password":password};
  var clienteMLabRaiz=requestjson.createClient(urlRaizMLab+"/Usuarios?"+apiKey);
  console.log(urlRaizMLab+"/Usuarios?"+apiKey);
  clienteMLabRaiz.post('',params,function(err,resM,body){
    //console.log(body.message);
    if(resM.statusCode==400){
      res.status(400).send({"msg":"Error en la peticion"});
    }else{
      res.status(200).send(body);
    }
  })
})


app.get('/token',function(req,res){
  res.set ("Access-Control-Allow-Headers","Content-Type");
  var params = {
  'headers': {
    'Authorization': 'Basic YXBwLmJidmEuZXN0YWRpc3RpY2FzOlFLeElOTlZVMVhMVkEzMW8kUlhnZmZmWVJWJFk2KlpAcEdHc2Vqekdhd1RyZnJBUyRPJURXT2VMRUlwRlhReWY=',
    'Content-Type': 'application/json'
  }
};
  params=JSON.stringify(params);
  var clienteBBVA=requestjson.createClient(urlTokenBBVA);
  clienteBBVA.headers={
    "Authorization": "Basic YXBwLmJidmEuZXN0YWRpc3RpY2FzOlFLeElOTlZVMVhMVkEzMW8kUlhnZmZmWVJWJFk2KlpAcEdHc2Vqekdhd1RyZnJBUyRPJURXT2VMRUlwRlhReWY=",
    "Content-Type": "application/json"};
  //console.log(clienteBBVA.headers);
  clienteBBVA.post('',{},function(err,resM,body){
    if(resM.statusCode>=400){
      res.status(400).send({"msg":"Error en la peticion"});
    }else{
      res.status(200).send(body);
    }
  })
})

app.post('/results',function(req,res){
  res.set ("Access-Control-Allow-Headers","Content-Type");
  //console.log(req.body);
  var jwt=req.body.jwt;
  var lat=req.body.lat;
  var lng=req.body.lng;
  var es_code=req.body.es_code;
  var zip=req.body.zip;
  var anio=req.body.anio;
  var option=req.body.option;
  var urlBase;
  if(option==1){
    urlBase=urlTiles+lat+"/"+lng+"/basic_stats?$page_key=201501&$page_size=12&category="+es_code+"&cards=all&channel=pos&max_date=201512&min_date=201501";
  }else{
    urlBase=urlZip+zip+"/basic_stats?$page_key=201501&$page_size=12&category="+es_code+"&cards=all&channel=pos&group_by=month&max_date=201512&min_date=201501";
  }
  console.log(urlBase);
  var clienteBBVA=requestjson.createClient(urlBase);
  clienteBBVA.headers={
    "Authorization": "jwt "+jwt,
    "Accept": "application/json"};
  console.log(clienteBBVA.headers);
  clienteBBVA.get('',function(err,resM,body){
    if(resM.statusCode>=400){
      res.status(400).send(body);
    }else{
      console.log(resM);
      res.status(200).send(body);
    }
  })
})

app.post('/update',function(req,res){
  res.set ("Access-Control-Allow-Headers","Content-Type");
  var email=req.body.email.trim();
  var password=req.body.password;
  var params={ "$set" : { "password" : password } };
  var clienteMLabRaiz=requestjson.createClient(urlRaizMLab+"/Usuarios/"+email+"?"+apiKey);
  console.log(email);
  console.log(urlRaizMLab+"/Usuarios/"+email+"?"+apiKey);
  clienteMLabRaiz.put('',params,function(err,resM,body){
    //console.log(body.message);
    if(resM.statusCode>=400){
      res.status(400).send({"msg":"Error en la peticion"});
    }else{
      res.status(200).send(body);
    }
  })
})
